{-# LANGUAGE TemplateHaskell #-}
module Reddit.Login
  ( login, loginOAuth, loginOAuthTest, AccessToken(..) ) where

import Reddit.Types.Reddit

import Data.Text (Text)
import Network.API.Builder hiding (runRoute)
import Data.Aeson.TH
import Data.Aeson.Types (Value)

loginRoute :: Text -> Text -> Route
loginRoute user pass = Route [ "api", "login" ]
                             [ "rem" =. True
                             , "user" =. user
                             , "passwd" =. pass ]
                             "POST"

loginRouteOAuth :: Text -> Text -> Route
loginRouteOAuth user pass = Route [ "api", "v1", "access_token" ]
                                  [ "grant_type" =. ("password" :: Text)
                                  , "username"   =. user
                                  , "password" =. pass ]
                                  "POST"

data AccessToken = AccessToken { access_token :: String
                               , token_type   :: Text
                               , expires_in   :: Integer
                               , scope        :: Text
                               } deriving (Read, Show, Eq)

loginOAuth :: Monad m => Text -> Text -> RedditT m AccessToken
loginOAuth user pass = runRoute $ loginRouteOAuth user pass

loginOAuthTest :: Monad m => Text -> Text -> RedditT m Value
loginOAuthTest user pass = runRoute $ loginRouteOAuth user pass

getLoginDetails :: Monad m => Text -> Text -> RedditT m LoginDetails
getLoginDetails user pass = receiveRoute $ loginRoute user pass

-- | Make a login request with the given username and password.
login :: Monad m
      => Text -- ^ Username to login with
      -> Text -- ^ Password to login with
      -> RedditT m LoginDetails
login = getLoginDetails

deriveJSON defaultOptions ''AccessToken
