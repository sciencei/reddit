module Reddit.Types.Report where

import Reddit.Types.User

import Control.Applicative
import qualified Data.Vector as V
import Data.Aeson
import Data.Monoid
import Data.Text (Text)
import Prelude

class Report a where
    body :: a -> Text

data ModReport = ModReport { modReportBody :: Text, author :: Username }
  deriving (Show, Read, Eq)
data UserReport = UserReport { userReportBody :: Text, count :: Int }
  deriving (Show, Read, Eq)

instance Report ModReport where
  body = modReportBody

instance FromJSON ModReport where
  parseJSON (Array as) = do
    body <- parseJSON (V.head as)
    let text = case body of
                Nothing -> ""
                Just x  -> x
    ModReport <$> return text
              <*> parseJSON (V.last as)
  parseJSON _ = mempty
    
instance Report UserReport where
  body = userReportBody

instance FromJSON UserReport where
  parseJSON (Array as) = do
    body <- parseJSON (V.head as)
    let text = case body of
                Nothing -> ""
                Just x  -> x
    UserReport <$> return text
               <*> parseJSON (V.last as)
  parseJSON _ = mempty
